# Dyson App test Using Appium And Serenity #

Serenity is a wrapper over selenium and it helps creating a beautiful report that is understandable by all the stake holders. 

### What is this repository for? ###

This repository contains the framework for testing the connectivity app on any platform depending on the configuration in the serenity.properties file or from CI tool(Jenkins) 

### How do I get set up? ###

This is a maven project.

You need to provide the information from the config file. Its the serenity.properties file.(Don't Change the name of this file)
All these information could later be provided from the Jenkins or Codebuild. 

#### Dependencies ####
* serenity core
* serenity cucumber 5
* junit

#### Database configuration ####
Not Required

#### How to run tests ####

##### Steps: #####
1. Download and install node js
2. Download and install android studio (For windows)
    You can install Xcode from App Store.
3. Download appium client and appium server 

Before Executing the test make sure you have following up and Running.

1. Start Appium Server(The Code can start the server as well)
2. Connect the Mobile Device and enable the USB Debugging mode.(The code can start the emulator , but it takes lot of time. Better to keep it open.)

If you want to change to emulator, Update the property File

Execute the test with command :

mvn install -Dcucumber.options="src/test/resources/features/" serenity:aggregate

##### Test Result #####

The result could be found inside the folder target/serenity/site/index.html once the test completes running.