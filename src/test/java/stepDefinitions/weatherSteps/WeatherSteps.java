package stepDefinitions.weatherSteps;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.ConnectivityApp.WeatherHomePage;

public class WeatherSteps extends WeatherHomePage {

    @Given("I have an internet connection")
    public void i_have_an_internet_connection() {
        getConnectionStatus();
    }

    @When("I have successfully downloaded the weather data")
    public void i_have_successfully_downloaded_the_weather_data() {
        downloadWeatherData();
    }

    @Then("I can see the weather and temperature for today")
    public void i_can_see_the_weather_and_temperature_for_today() {
        viewResults();
    }
}
