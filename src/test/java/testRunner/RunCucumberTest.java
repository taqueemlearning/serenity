package testRunner;



import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;


@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = {"src/test/resources/features/weather"}, glue = {"stepDefinitions"},
/* plugin = {"pretty", "html:target/cucumber-reports/"},*/ dryRun = false)

public class RunCucumberTest {


}
