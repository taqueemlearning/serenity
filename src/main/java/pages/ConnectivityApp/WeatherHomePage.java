package pages.ConnectivityApp;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

public class WeatherHomePage extends CommonPage {

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='fetch_data_title_label']")
    @AndroidFindBy(id = "connection_display_title")
    protected WebElementFacade textConnectionStatus;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Fetch Data']")
    @AndroidFindBy(id = "connection_display_primary_button")
    protected WebElementFacade buttonFetchData;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='view_results_title_label']")
    @AndroidFindBy(id = "fetch_data_title")
    protected WebElementFacade textDataCollected;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='View Results']")
    @AndroidFindBy(id = "fetch_data_primary_button")
    protected WebElementFacade buttonViewResults;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='weather_description_label']")
    @AndroidFindBy(id = "weather_display_weather_value")
    protected WebElementFacade textWeather;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='temperature_title_label']")
    @AndroidFindBy(id = "weather_display_temperature_value")
    protected WebElementFacade textTemperature;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Done']")
    @AndroidFindBy(id = "weather_display_primary_button")
    protected WebElementFacade buttonDone;

    protected void getConnectionStatus() {

        Assert.assertEquals("The internet connection status is incorrect", "Connected", textConnectionStatus.getText());
        System.out.println("Status is "+ textConnectionStatus.getText());
    }

    protected void downloadWeatherData() {
        clickOn(buttonFetchData);
        Assert.assertTrue("Weather data is unable to download", textDataCollected.isDisplayed());
    }

    protected void viewResults() {
        buttonViewResults.click();
        Assert.assertTrue("Weather data is not displayed", textWeather.isDisplayed());
        Assert.assertTrue("Temperature  data is not displayed", textTemperature.isDisplayed());
        buttonDone.click();
    }


}
