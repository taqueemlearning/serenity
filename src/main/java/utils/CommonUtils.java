package utils;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.SerenitySystemProperties;
import net.thucydides.core.ThucydidesSystemProperty;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class CommonUtils {


    AppiumDriverLocalService service;

    protected void startServer() {
        service = AppiumDriverLocalService.buildDefaultService();
        service.start();
    }

    protected void stopServer() {
        if (service.isRunning()) {
            System.out.println("Server is running");
        }
        service.stop();
    }


    protected void startEmulator() {

        String deviceName = SystemEnvironmentVariables.createEnvironmentVariables().getProperty("appium.deviceName");
        if (!deviceName.equals("Android Device")) {
            String path = Constants.pathEmulatorStartBatch;
            try {
                Runtime.getRuntime().exec(path);
                Thread.sleep(60000);
            } catch (Exception E) {
                E.printStackTrace();
            }
        }
    }
}
